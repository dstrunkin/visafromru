var global_flag = 0;
$(document).ready(function () {
  $('#myModal1').modalSteps({
    btnPreviousHtml: 'Назад',
    btnNextHtml: 'Далее',
    btnLastStepHtml: 'Оплатить',
    callbacks: {
      '1': function () {
        $('#submit_form_button').hide();
        $('#submit').hide();
        $('#fake-button').show();
        $('.modal-content').removeClass('payment-color');
        $('.modal-footer').addClass('first-page');
      },
      '2': function () {
        $('#submit_form_button').show();
        $('#submit').hide();
        $('#fake-button').hide();
        $('.modal-content').removeClass('payment-color');
        $('.modal-footer').removeClass('first-page');
        ga('send', 'event', 'Knopka', 'SendAnketa', window.location.href); 
        yaCounter47121678.reachGoal('sendanketa');
      },
      '3': function () {
        setTimeout(function () { $('#submit_form_button').hide(); }, 0);
        $('#submit').show();
        $('#fake-button').hide();
        $('.modal-content').addClass('payment-color');
        $('.modal-footer').removeClass('first-page');
        ga('send', 'event', 'Knopka', 'SendUslugi', window.location.href); 
        yaCounter47121678.reachGoal('daleeuslugi');
      }
    },
    completeCallback: function () {
      $('.modal-content').removeClass('payment-color');
      $('.modal-footer').removeClass('first-page');
    }
  });

  $('.date-input').datetimepicker({
    viewMode: 'days',
    format: 'DD.MM.YYYY'
  });
    
  $('.date-input-pass').datetimepicker({
    viewMode: 'days',
    format: 'DD.MM.YYYY',
    widgetPositioning: {
            horizontal: 'auto',
            vertical: 'bottom'
         }

  });

  $('#toDate').data("DateTimePicker").useCurrent(false);

  $("#fromDate").on("dp.change", function (e) {
    $('#toDate').data("DateTimePicker").minDate(e.date);
  });
  $("#toDate").on("dp.change", function (e) {
    $('#fromDate').data("DateTimePicker").maxDate(e.date);
  });

  $('#submit_form_button').on('click', function () {
    if (window.SendUslugi == undefined) {
      window.SendUslugi = true;
    }
  });

  $('#fake-button').on('click', function (e) {
    ressetAll();
    validate($('div.questionnaire'));
  });

  function showError(elems, message) {
    elems.css("border", "1px solid red");
    elems.after('<div class="validate-info" style="position: relative"><span style="position:absolute; top: 0; left: 0; color: red;">'+message+'</span></div>');
  }

  function resetError(elems) {
    elems.css("border", "1px solid #ccc");
    $('.validate-info', elems.parent()).detach();
  }

  function ressetAll() {
    $('.validate-info', $('div.questionnaire')).each(function (i, elem) {
      $(elem).detach();
    });
  }

  function validate(form) {
    var count = 0;
    $('input', form).each(function (i, elem) {
      var obj = $(elem);
      if (obj.prop('name').length > 10) {
        resetError(obj);
        var value = obj.val().trim()
        if (!value) {
          showError(obj, 'Введите данные');
          count++;
        } else if(obj.is('#exampleInputName1') && !~value.indexOf(' ')) {
          showError(obj, 'Введите Фамилию и Имя');
          count++;
        } else if(obj.is('#exampleInputName3') && /[^\sa-zа-яё]/gi.test(value)) {
          showError(obj, 'Введите только буквы и пробелы');
          count++;
        } else if(obj.is('#exampleInputName4') && /[^\sa-zа-яё]/gi.test(value)) {
          showError(obj, 'Введите только буквы и пробелы');
          count++;
        } else if(obj.is('#exampleInputName6') && !/[\w]/gi.test(value)) {
          showError(obj, 'Введите только цифры и латиницу');
          count++;
        } else if(obj.is('#exampleInputName8') && !/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(value)) {
          showError(obj, 'Введите правильный email');
          count++;
        } else if(obj.is('#exampleInputName9') && !/[\d]/gi.test(value)) {
          showError(obj, 'Введите только цифры');
          count++;
        } else if(obj.is('#exampleInputName10') && /[^\sa-zа-яё]/gi.test(value)) {
          showError(obj, 'Введите только буквы и пробелы');
          count++;
        } else if(obj.is('#exampleInputName11') && !/[\d]/gi.test(value)) {
          showError(obj, 'Введите только цифры');
          count++;
        }
      }
    });

    if (count === 0) {
      $('#submit_form_button').click();
      $('#submit_form_button').show();
      $('#fake-button').hide();
    }

    return false;
  }
});