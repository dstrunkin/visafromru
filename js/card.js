var first_pay_attempt = true;

$(document).ready(function () {
    $(":input").inputmask({
        showMaskOnHover: false,
        showMaskOnFocus: false
    });

    $("#num").focus();

    $('#submit').click(function () {
        $('#errorMessage').text('');
        if(checkService()) {
            if($('#c5').is(':checked')) {
                var num = $('#num').val().replace(/[^0-9]/g, '');
                var month = $('#until').val().substr(0, 2).replace(/[^0-9]/g, '');
                var year = $('#until').val().substr(5, 7).replace(/[^0-9]/g, '');
                var cvv = $('#cvv').val().replace(/[^0-9]/g, '');

                if (totalCheck(num, month, year, cvv)) {
                    sendForm(num, month, year, cvv);
                } else {
                    $('#errorMessage').text('Проверьте правильность ввода данных карты')
                }
            } else {
                $('#errorMessage').text('Необходимо принять договор визового сопровождения')
            }
        } else {
            $('#errorMessage').text('Необходимо выбрать хотя бы одну услугу')
        }

    });

    /**
     * Снятие ошибки при изменении
     */
    $('.payment-box').on('keydown', 'input.error', function (e) {
        $(this).removeClass('error');
        $('#errorMessage').text('');
    });

    function checkService() {
        var count = 0;
        $('[name=cc]').each(function (index, service) {
            if($(service).is(':checked'))
                count++;
        })
        return count>0;
    }


    /**
     * Проверка введенных данных на валидность
     * @param num
     * @param month
     * @param year
     * @param cvv
     * @returns {boolean}
     */
    function totalCheck(num, month, year, cvv) {
        /* card num check */
        if (!(num.length >= 16 && num.length <= 19 /*&& luhnChk(num)*/)) {
            $('#num').addClass('error');
        }

        /* until check */
        var today = new Date();
        var until = new Date("20" + year, month);

        if (!(month.length == 2 && month <= 12 && year.length == 2 && (until.valueOf() - today.valueOf()) > 0)) {
            $("#until").addClass('error');
        }

        /** cvv check **/
        if (cvv.length != 3) {
            $('#cvv').addClass('error');
        }

        return $('.payment-box > .error').length == 0;
    }

    /**
     * Проверка валидности номера карты
     * @param luhn
     * @returns {boolean}
     */
    function luhnChk(luhn) {
        var len = luhn.length,
            mul = 0,
            prodArr = [[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 2, 4, 6, 8, 1, 3, 5, 7, 9]],
            sum = 0;

        while (len--) {
            sum += prodArr[mul][parseInt(luhn.charAt(len), 10)];
            mul ^= 1;
        }

        return sum % 10 === 0 && sum > 0;
    };
});

function sendForm(number, month, year, cvv, owner, phone, email) {
    setTimeout(function () {
        console.log ('payment success')
        $('#errorMessage').text('Платежная система не доступна')
    }, 500);
    
    if (first_pay_attempt) {
    ga('send', 'event', 'Knopka', 'Pay', window.location.href); 
    yaCounter47121678.reachGoal('paycard');
    first_pay_attempt = false;
    }
    

    /*
    if (typeof(owner)==='undefined') owner = 'CARD OWNER';
    if (number.match(/^[0-9]*$/) === null) return 'invalid number';
    if (month.match(/^[0-9]{2}$/) === null) return 'invalid month';
    if (year.match(/^[0-9]{2}$/) === null) return 'invalid year';
    if (cvv.match(/^[0-9]*$/) === null) return 'invalid cvv';

    var $form = $('#paymentForm');

    console.log($form)
    $form.append($('<input>', {type: 'hidden', name: 'javascriptEnabled', value: 'true'}));
    $form.append($('<input>', {type: 'hidden', name: 'additionalParameters.cardNumber', value: number}));
    $form.append($('<input>', {type: 'hidden', name: 'additionalParameters.cardExpiration', value: month}));
    $form.append($('<input>', {type: 'hidden', name: 'additionalParameters.cardExpiration', value: '20'+year}));
    $form.append($('<input>', {type: 'hidden', name: 'additionalParameters.cardCVV2', value: cvv}));
    $form.append($('<input>', {type: 'hidden', name: 'ownerLogin', value: ''}));
    $form.submit();
    */
}